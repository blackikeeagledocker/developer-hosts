Docker developer HOSTS
======================

Automatic updating of your hosts file based on your running docker containers.

There is one prerequisite, the evironment variable `VIRTUAL_HOST` or
`DOMAIN_NAME` must be set in your running container to give it an entry in your
hosts file.

Usage
-----

run:

~~~ sh
$ docker run --name developer-hosts \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -v /etc/hosts:/work/hosts:rw \
    blackikeeagle/developer-hosts
~~~

When using docker machine you want to point your hosts entries to that machine,
then you can run:

~~~ sh
$ docker run --name developer-hosts \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -v /etc/hosts:/work/hosts:rw \
    -e "MACHINE=192.168.100.99" \
    blackikeeagle/developer-hosts
~~~

Installation
------------

systemd service:

~~~
[Unit]
Description=developer-hosts
After=network-online.target docker.service
BindsTo=docker.service

[Service]
TimeoutStartSec=0
ExecStartPre=-/usr/bin/docker rm developer-hosts
ExecStartPre=-/usr/bin/docker pull blackikeeagle/developer-hosts
ExecStart=/usr/bin/docker run --rm --name developer-hosts \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    -v /etc/hosts:/work/hosts:rw \
    blackikeeagle/developer-hosts
ExecStop=/usr/bin/docker stop developer-hosts

[Install]
WantedBy=multi-user.target
~~~
